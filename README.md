## SiaClassic Hub

## Related links
- [Laravel 5.4 docs](https://laravel.com/docs/5.4)

## Installation

### Web
- ``git clone https://gitlab.com/jinxianjo/siaclassic-hub.git``
- ``npm i``
- ``npm run production``
- ``composer update``
- ``php artisan vendor:publish``
- ``php artisan key:generate``
- Make and edit .env config (check .env.example)
- ``php artisan migrate``
- Done!


## License

Available under [the MIT license](http://mths.be/mit).
